#/bin/sh

# This file processes the bam file 1--NA12878-OD1.bam using multiple tools and options to compress
# and uncompress it. The numbers in the report are two times the number output by this script because
# for the 39X sequence data there are two BAM files of this size.

date
echo "Scramble lossy 10 threads"

time scramble -I bam -O cram -r /roslin-test/ally/reference/human_g1k_v37.fasta -B -t 10 1--NA12878-OD1.bam 1--NA12878-OD1.bam.lossy10Threads.cram

date
echo "Scramble lossy 1 thread"


time scramble -I bam -O cram -r /roslin-test/ally/reference/human_g1k_v37.fasta -B -t 1 1--NA12878-OD1.bam 1--NA12878-OD1.bam.lossy1Threads.cram

date
echo "scramble 10 theads"


time scramble -I bam -O cram -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 10 1--NA12878-OD1.bam 1--NA12878-OD1.bam.10Threads.cram

date
echo "scramble 1 thread"


time scramble -I bam -O cram -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 1 1--NA12878-OD1.bam 1--NA12878-OD1.bam.1Threads.cram

date
echo "scramble uncompress lossy 10 threads"


time scramble -I cram -O bam -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 10 -m 1--NA12878-OD1.bam.lossy10Threads.cram 1--NA12878-OD1.bam.lossy10Threads.cram.bam

date
echo "scramble uncompress lossy 1 thread"

time scramble -I cram -O bam -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 1 -m 1--NA12878-OD1.bam.lossy1Threads.cram 1--NA12878-OD1.bam.lossy1Threads.cram.bam

date
echo "scramble uncompress 10 threads"


time scramble -I cram -O bam -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 10 -m 1--NA12878-OD1.bam.10Threads.cram 1--NA12878-OD1.bam.10Threads.cram.bam

date
echo "scramble uncompress 1 threads"

time scramble -I cram -O bam -r /roslin-test/ally/reference/human_g1k_v37.fasta -t 1 -m 1--NA12878-OD1.bam.10Threads.cram 1--NA12878-OD1.bam.1Threads.cram.bam

date
echo "samtools 1 thread"

time samtools view -C -o 1--NA12878-OD1.bam.samtools1Threads.cram -T /roslin-test/ally/reference/human_g1k_v37.fasta 1--NA12878-OD1.bam

date
echo "samtools 10 threads"

time samtools view -C -o 1--NA12878-OD1.bam.samtools10Threads.cram -T /roslin-test/ally/reference/human_g1k_v37.fasta -@ 10  1--NA12878-OD1.bam


date
echo "samtools uncompress 1 thread"

time samtools view -b -o 1--NA12878-OD1.bam.samtools1Threads.cram.bam -T /roslin-test/ally/reference/human_g1k_v37.fasta 1--NA12878-OD1.bam.samtools1Threads.cram

date
echo "samtools uncompress 10 threads"

time samtools view -b -o 1--NA12878-OD1.bam.samtools10Threads.cram.bam -T /roslin-test/ally/reference/human_g1k_v37.fasta -@ 10  1--NA12878-OD1.bam.samtools10Threads.cram

date
echo "cramtools lossy 1 thread"

time java -Xmx50g -jar /roslin/edingenomics/software/cram/cramtools-2.1.jar cram --input-bam-file 1--NA12878-OD1.bam --reference-fasta-file /roslin-test/ally/reference/human_g1k_v37.fasta --output-cram-file 1--NA12878-OD1.bam.cramtoolsLossy.cram --lossy-quality-score-spec "*8" --capture-all-tags --preserve-read-names

date
echo "cramtools 1 thread"

time java -Xmx50g -jar /roslin/edingenomics/software/cram/cramtools-2.1.jar cram --input-bam-file 1--NA12878-OD1.bam --reference-fasta-file /roslin-test/ally/reference/human_g1k_v37.fasta --output-cram-file 1--NA12878-OD1.bam.cramtools.cram --capture-all-tags --preserve-read-names --lossless-quality-score

date
echo "cramtools uncompress lossy 1 thread"

time java -Xmx50g -jar /roslin/edingenomics/software/cram/cramtools-2.1.jar bam --input-cram-file 1--NA12878-OD1.bam.cramtoolsLossy.cram --reference-fasta-file /roslin-test/ally/reference/human_g1k_v37.fasta --output-bam-file 1--NA12878-OD1.bam.cramtoolsLossy.cram.bam --calculate-md-tag --calculate-nm-tag

date
echo "cramtools uncompress 1 thread"

time java -Xmx50g -jar /roslin/edingenomics/software/cram/cramtools-2.1.jar bam --input-cram-file 1--NA12878-OD1.bam.cramtools.cram --reference-fasta-file /roslin-test/ally/reference/human_g1k_v37.fasta --output-bam-file 1--NA12878-OD1.bam.cramtools.cram.bam --calculate-md-tag --calculate-nm-tag

date

