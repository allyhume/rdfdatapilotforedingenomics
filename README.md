RDF Data Pilot For Edinburgh Genomics
=====================================

This repository contains scripts and other files associated with the RDF
Data Pilot that worked with Edinburgh Genomics.

The work was done by Ally Hume of EPCC, The University of Edinburgh in 2014.
